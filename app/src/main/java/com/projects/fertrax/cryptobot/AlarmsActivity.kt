package com.projects.fertrax.cryptobot

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.projects.fertrax.cryptobot.adapters.AlarmAdapter
import com.projects.fertrax.cryptobot.adapters.AlarmList
import kotlinx.android.synthetic.main.activity_alarms.*
import kotlinx.android.synthetic.main.app_bar_main2.*
import org.json.JSONObject

class AlarmsActivity : Main2Activity() {

    private var listCoin = hashMapOf("All coins" to "all")
    private val listIndicators = hashMapOf("One Ema" to "one_ema",
            "Double Ema" to "double_ema",
            "RSI" to "rsi",
            "Stochastic" to "stochastic",
            "Parabolic Sar" to "sar",
            "Percentage" to "percentage")
    private var listRules = hashMapOf("Below" to "below","Above" to "above","cross" to "Crosses")
    private var listStatus = arrayOf("new","review","status")
    private var listPeriod = arrayOf("1m","3m","5m","15m","1h","2h","4h","6h","8h","12h","1d","3d","1w","1M")
    private lateinit var alarmList : MutableList<AlarmList>
    private var listCoinVal = ArrayList(listCoin.keys)
    private var listIndicatorsVal = ArrayList(listIndicators.keys)
    private var listRulesVal = ArrayList(listRules.keys)
    private val gson = GsonBuilder().setPrettyPrinting().create()

    override fun getLayoutId(): Int = R.layout.activity_alarms
    override fun getCustomTitle(): String? = "Alarms"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_alarms)
        //Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M


        ssCoin.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listCoinVal)
        spaIndicators.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listIndicatorsVal)
        spaCondition.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listRulesVal)
        spaStatus.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listStatus)
        spaPeriod.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listPeriod)

        rvAlarms.layoutManager = LinearLayoutManager(this)

        alarmList = mutableListOf()

        Volley("https://kypo.gameplux.com/api.php?op=alarm").sendData(this, ::getAction){
            val params = HashMap<String,String>()
            params["action"] = "read"
            params
        }
        spaIndicators.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                etaValues.visibility = LinearLayout.GONE
                lnValues.visibility = LinearLayout.GONE
                etaValues.setText("0.00")

                when (spaIndicators.selectedItem) {
                    "One Ema" -> {
                        //one ema
                        listRules = hashMapOf("Below" to "below", "Above" to "above")
                        lnValues.visibility = LinearLayout.VISIBLE
                        etaValues.visibility = LinearLayout.VISIBLE
                    }
                    "Double Ema" -> {
                        //double ema
                        listRules = hashMapOf("Crosses Up" to "1", "New Crosses Up" to "2", "Crosses Down" to "0")
                    }
                    "RSI" -> {
                        //rsi
                        listRules = hashMapOf("Below" to "below","Above" to "above","Crosses" to "cross","Crosses Up" to "prev")
                        lnValues.visibility = LinearLayout.VISIBLE
                        etaValues.visibility = LinearLayout.VISIBLE
                    }
                    "Stochastic" -> {
                        //stochastic
                        listRules = hashMapOf("Crosses Below 20" to "1is1",
                                "Crosses Above 80" to "1is0",
                                "Above 80" to "2is1",
                                "Below 80" to "2is0",
                                "Falling Line Above 80" to "3is1")
                    }
                    "Parabolic Sar" -> {
                        //parabolic sar
                        listRules = hashMapOf("Lower At Close" to "1", "Greater At Close" to "0")
                    }
                    "Percentage" -> {
                        //percentage
                        listRules = hashMapOf("Below" to "below","Above" to "above")
                        lnValues.visibility = LinearLayout.VISIBLE
                        etaValues.visibility = LinearLayout.VISIBLE
                    }
                }
                listRulesVal = ArrayList(listRules.keys)
                spaCondition.adapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, listRulesVal)
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        btaQtyMinus.setOnClickListener{
            var digits = Math.pow(10.0,2.toDouble()).toInt()
            var v = 0.00
            if (!etaValues.text.isEmpty()) v = etaValues.text.toString().toDouble()
            var n = v * digits

            if (n > 0 || spaIndicators.selectedItem == "Percentage") {
                n = (n.minus(1)).div(digits)
            }

            etaValues.setText(String.format("%.2f",n))
        }
        btaQtyPlus.setOnClickListener {
            var digits = Math.pow(10.0,2.toDouble()).toInt()
            var v = 0.00
            if (!etaValues.text.isEmpty()) v = etaValues.text.toString().toDouble()

            var n = v * digits
            n = (n.plus(1)).div(digits)
            etaValues.setText(String.format("%.2f", n))
        }
        btaSave.setOnClickListener {
            //AlarmAdapter
            val coin = ssCoin.selectedItem.toString()
            val indicator = spaIndicators.selectedItem.toString()
            val condition = spaCondition.selectedItem.toString()
            val textFinal = "${spaStatus.selectedItem} ${spaPeriod.selectedItem} $indicator"
            val value =
                    if (etaValues.text.isEmpty() || (indicator != "One ema" && indicator != "RSI" && indicator != "Percentage")) ""
                    else etaValues.text.toString()
            Volley("https://kypo.gameplux.com/api.php?op=alarm").sendData(this,getParams = {
                obj ->
                    val body = obj.getString("body")
                    val alarm = AlarmList(coin, textFinal, condition, value, "1", body)
                    alarmList.add(alarm)
                    rvAlarms.adapter = AlarmAdapter(alarmList)
            }, setParams = ::createAction)
        }
    }

    private fun getAction(obj: JSONObject) {
        val body = obj.getJSONArray("body")
        val symbol: ArrayList<String> = gson.fromJson(obj.getString("symbols"), object : TypeToken<ArrayList<String>>() {}.type)
        listCoinVal.addAll(symbol)
        ssCoin.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listCoinVal)
        if (body.length() > 0) {
            for (i in 0 until body.length()) {
                val objAlarm = body.getJSONObject(i)
                var rule = hashMapOf("below" to "Below", "above" to "Above")
                var indicator = "One Ema"
                when (objAlarm.getString("indicator")) {
                    "double_ema" -> {
                        rule = hashMapOf("1" to "Crosses Up" , "2" to "New Crosses Up", "0" to "Crosses Down")
                        indicator = "Double Ema"
                    }
                    "rsi" -> {
                        rule = hashMapOf("below" to "Below","above" to "Above","cross" to "Crosses","prev" to "Crosses Up")
                        indicator = "RSI"
                    }
                    "stochastic" -> {
                        rule = hashMapOf("1is1" to "Crosses Below 20",
                                "1is0" to "Crosses Above 80",
                                "2is1" to "Above 80",
                                "2is0" to "Below 80",
                                "3is1" to "Falling Line Above 80")
                        indicator = "Stochastic"
                    }
                    "sar" -> {
                        rule = hashMapOf("1" to "Lower At Close", "0" to "Greater At Close")
                        indicator = "Parabolic Sar"
                    }
                    "percentage" -> indicator = "Percentage"

                }
                val coin =
                        if (objAlarm.getString("coin") == "all") "All Coins"
                        else objAlarm.getString("coin")
                val value =
                        if (objAlarm.getString("vals").isEmpty() || objAlarm.getString("vals") == "null") ""
                        else objAlarm.getString("vals")
                val active = objAlarm.getString("active")
                val textFinal = "${objAlarm.getString("status")} ${objAlarm.getString("period")} $indicator"

                val alarm = AlarmList(coin, textFinal, rule[objAlarm.getString("rule")].toString(), value, active, objAlarm.getString("id"))
                alarmList.add(alarm)
                if (alarmList.size > 0) rvAlarms.adapter = AlarmAdapter(alarmList)
            }
        }
    }

    private fun createAction(): Map<String,String> {
        val params = HashMap<String, String>()

        params["action"] = "create"
        params["coin"] = if (ssCoin.selectedItem == "All coins") "all" else ssCoin.selectedItem.toString()
        params["status"] = spaStatus.selectedItem.toString()
        params["period"] = spaPeriod.selectedItem.toString()
        params["indicator"] = listIndicators[spaIndicators.selectedItem].toString()
        params["condition"] = listRules[spaCondition.selectedItem].toString()

        if (etaValues.visibility == LinearLayout.VISIBLE)
            params["values"] = etaValues.text.toString()

        return params
    }
}