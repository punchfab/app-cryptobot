package com.projects.fertrax.cryptobot

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.ArrayAdapter
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.projects.fertrax.cryptobot.adapters.BlockAdapter
import com.projects.fertrax.cryptobot.adapters.BlockList
import kotlinx.android.synthetic.main.activity_block.*
import org.json.JSONObject

class BlockActivity : Main2Activity() {
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private lateinit var listCoins: ArrayList<String>
    private lateinit var blockList : MutableList<BlockList>
    private var symbols = HashMap<String,String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        blockList = mutableListOf()
        rvBlock.layoutManager = LinearLayoutManager(this)
        listCoins = arrayListOf()
        Volley("https://kypo.gameplux.com/api.php?op=getBlock").getData(this, ::getAction)

        btBlockSave.setOnClickListener {
            val coin = sbCoin.selectedItem.toString()
            val price = symbols[sbCoin.selectedItem.toString()].toString()
            val block = BlockList(coin,price)
            blockList.add(block)
            rvBlock.adapter = BlockAdapter(blockList)
            Volley("https://kypo.gameplux.com/api.php?op=setBlock").sendData(this,{},::blockAction)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_block
    override fun getCustomTitle(): String? = "Block Coins"

    private fun getAction(obj: JSONObject) {
        symbols = gson.fromJson(obj.getString("symbols"), object : TypeToken<HashMap<String,String>>() {}.type)
        listCoins.addAll(symbols.keys)
        sbCoin.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listCoins)
        blockList = gson.fromJson(obj.getString("body"), object : TypeToken<List<BlockList>>() {}.type)
        rvBlock.adapter = BlockAdapter(blockList)
    }

    private fun blockAction(): Map<String,String> {
        val params = HashMap<String,String>()
        params["action"] = "block"
        params["symbol"] = sbCoin.selectedItem.toString()
        return params
    }
}
