package com.projects.fertrax.cryptobot

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.projects.fertrax.cryptobot.adapters.OrderList
import com.projects.fertrax.cryptobot.adapters.OrderListAdapter
import kotlinx.android.synthetic.main.activity_buy.*
import org.json.JSONObject

class BuyActivity : AppCompatActivity() {
    private lateinit var rvListRules : RecyclerView
    private lateinit var sectionLimit: LinearLayout
    private lateinit var params: LinearLayout.LayoutParams
    private val list = arrayListOf("Limit Order", "Market Order", "Rule")
    private val indicators = arrayListOf("one ema", "double ema", "rsi", "stochastic", "parabolic sar")
    private var rules = arrayListOf("Below","Above","Crosses","Crosses Up","Crosses Down","Equal")
    private lateinit var orderList : MutableList<OrderList>
    private var entryPrice: String = ""
    private var jsonOrderList: String = ""
    private val gson = GsonBuilder().setPrettyPrinting().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy)

        supportActionBar!!.title = "Buy ${intent.getStringExtra("symbol")}"
        val id = intent.getStringExtra("id")
        Volley("https://kypo.gameplux.com/api.php?op=getOnly&id=$id").getData(this, ::getAction)

        sectionLimit = findViewById(R.id.sectionLimit)
        params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

        val adapterOrders = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
        val adapterIndicators = ArrayAdapter(this, android.R.layout.simple_spinner_item, indicators)
        val adapterRules = ArrayAdapter(this, android.R.layout.simple_spinner_item, rules)
        orders_spinner.adapter = adapterOrders
        indicator_sp.adapter = adapterIndicators
        rule_sp.adapter = adapterRules

        orders_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                Toast.makeText(applicationContext, orders_spinner.selectedItem.toString(), Toast.LENGTH_SHORT).show()
                when (position) {
                    0 -> {
                        addLimitContent()
                    }
                    1 -> {
                        addMarketContent()
                    }
                    2 -> {
                        addRuleContent()
                    }
                }
                //Toast.makeText(applicationContext, position.toString(), Toast.LENGTH_SHORT).show()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        var myHandler: View.OnClickListener =  View.OnClickListener {
            v ->
            var price = etPriceLimit.text.toString()
            var amount = etQtyLimit.text.toString()

            when (v.id) {
                R.id.limit_plus_btn -> {
                    price = plusPrice(price)
                    etPriceLimit.setText(price)
                }

                R.id.limit_minus_btn -> {
                    price = minusPrice(price)
                    etPriceLimit.setText(price)
                }

                R.id.qty_plus_btn ->
                    etQtyLimit.setText(plusPrice(amount, 0))

                R.id.qty_minus_btn ->
                    etQtyLimit.setText(minusPrice(amount, 0))
            }
        }
        limit_plus_btn.setOnClickListener(myHandler)
        limit_minus_btn.setOnClickListener(myHandler)
        qty_plus_btn.setOnClickListener(myHandler)
        qty_minus_btn.setOnClickListener(myHandler)

        btnBuy.setOnClickListener {
            Volley("https://kypo.gameplux.com/api.php?op=setOrder&id=$id").sendData(this, setParams = ::sendAction)
            //Toast.makeText(this, orders_spinner.selectedItemPosition.toString(), Toast.LENGTH_SHORT).show()
        }

        rvListRules = findViewById(R.id.list_rules)
        rvListRules.layoutManager = LinearLayoutManager(this)
        orderList = mutableListOf()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                // Respond to the action bar's Up/Home button
                val dat = Intent()
                dat.putExtra("id", intent.getStringExtra("id"))
                setResult(Activity.RESULT_OK, dat)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addLimitContent () {
        sectionLimit.visibility = LinearLayout.VISIBLE
        order_limit.visibility = LinearLayout.VISIBLE
        order_amount.visibility = LinearLayout.VISIBLE
        order_price.visibility = LinearLayout.GONE
        sectionIndicators.visibility = LinearLayout.GONE
    }

    private fun addMarketContent () {
        order_price.visibility = LinearLayout.VISIBLE
        order_amount.visibility = LinearLayout.VISIBLE
        order_limit.visibility = LinearLayout.GONE
        sectionIndicators.visibility = LinearLayout.GONE
    }

    private fun addRuleContent () {
        order_amount.visibility = LinearLayout.VISIBLE
        sectionIndicators.visibility = LinearLayout.VISIBLE
        order_limit.visibility = LinearLayout.GONE
        order_price.visibility = LinearLayout.GONE

        lateinit var ruleName: String
        lateinit var ruleType: String

        //"one ema", "double ema", "rsi", "stochastic", "parabolic sar"
        //"Below","Above","Crosses","Crosses Up","Crosses Down","Equal"
        indicator_sp.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                rule_sp.visibility = LinearLayout.VISIBLE
                iValues.visibility = LinearLayout.GONE
                when (position) {
                    0 -> {
                        //one ema
                        //status 1: above, 0 below
                        rules = arrayListOf("Below","Above")
                        // 1 valor
                        iValues.visibility = LinearLayout.VISIBLE
                    }
                    1 -> {
                        //double ema
                        //status 1 crosses up, 2 new crosses up, 0 crosses down
                        rules = arrayListOf("Crosses Up","New Crosses Up","Crosses Down")
                        // ninguno
                    }
                    2 -> {
                        //rsi
                        /*
                        status below, above, crosses, prev crosses up
                         */
                        rules = arrayListOf("Below","Above","Crosses","Crosses Up")
                        // 1 valor
                        iValues.visibility = LinearLayout.VISIBLE
                    }
                    3 -> {
                        //stochastic
                        /*
                        * option
                        * 1 -> crosses below 20 status: 1, crosses above 80 status:0
                        * 2 -> above 80 status: 1, below 80 status: 0
                        * 3 -> falling line above 80
                        */

                        rules = arrayListOf("Crosses Below 20","Crosses Above 80","Above 80","Below 80","Falling Line Above 80")
                        // ninguno
                    }
                    4 -> {
                        //parabolic sar
                        /*
                        * lower at close
                        * greater at close
                         */
                        rules = arrayListOf("Lower At Close","Greater At Close")
                        // ninguno
                    }
                }
                val adapterRules = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_item, rules)
                rule_sp.adapter = adapterRules
                ruleName = indicators[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        rule_sp.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {ruleType = rules[position]}
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        save_rule.setOnClickListener{
            if (ruleName != null && ruleType != null) {
                val orderId = orderList.size + 1
                val orderValue =
                        if (iValues.text.isEmpty() || (ruleName != "one ema" && ruleName != "rsi")) "0"
                        else iValues.text.toString()
                val order = OrderList(ruleName, ruleType, orderId.toString(), orderValue)
                orderList.add(order)
                rvListRules.adapter = OrderListAdapter(orderList)
                //Toast.makeText(applicationContext,jsonOrderList,Toast.LENGTH_LONG).show()
            }
            else {
                Toast.makeText(applicationContext,"regla vacia",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun plusPrice(value: String, x: Int = 8): String {
        var digits = Math.pow(10.0,x.toDouble()).toInt()
        var v = 0.0
        if (!value.isEmpty()) v = value.toDouble()

        var price = v * digits
        price = (price.plus(1)).div(digits)
        return String.format("%.${x}f", price)
    }

    private fun minusPrice(value: String, x: Int = 8, m: Boolean = false): String {
        var digits = Math.pow(10.0,x.toDouble()).toInt()
        var v = 0.0
        if (!value.isEmpty()) v = value.toDouble()
        var price = v * digits

        if (price > 0 && !m || m) {
            price = (price.minus(1)).div(digits)
        }
        return String.format("%.${x}f", price)
    }

    private fun getAction(obj: JSONObject) {
        val body = obj.getJSONObject("body")
        val price = JSONObject(body.getString("price"))
        //val rules = JSONObject(body.getString("rules"))
        if (price.has("entry")) entryPrice = price.getString("entry")

        if (price.has("last")) {
            etPriceLimit.setText(price.getString("last"))
            tvPrice.text = price.getString("last")
        }

        if (body.getString("rules") != "null") {
            //Toast.makeText(applicationContext,"No hay reglas", Toast.LENGTH_SHORT).show()
            val rules = JSONObject(body.getString("rules"))
            if (rules.has("buy")) {
                val buy = JSONObject(rules.getString("buy"))
                if (buy.has("type") && buy.getString("type") != "null") orders_spinner.setSelection(buy.getString("type").toInt())

                if (buy.has("conditions") && buy.getString("conditions") != "null") {
                    orderList = gson.fromJson(buy.getString("conditions"), object : TypeToken<List<OrderList>>() {}.type)
                    if (orderList.size > 0) rvListRules.adapter = OrderListAdapter(orderList)
                }
            }
        }

        if (body.getString("data") != "null") {
            val data = JSONObject(body.getString("data"))
            if (data.has("amount")) etQtyLimit.setText(data.getString("amount"))
        }
    }

    private fun sendAction(): Map<String,String> {
        val params = HashMap<String, String>()
        jsonOrderList = gson.toJson(orderList)
        //"Limit Order", "Market Order", "Stop-Limit", "Rule"
        params["type"] = orders_spinner.selectedItemPosition.toString()
        params["amount"] = etQtyLimit.text.toString()
        params["action"] = "buy"

        if (orders_spinner.selectedItemPosition != 1) params["priceLimit"] = etPriceLimit.text.toString()
        if (orders_spinner.selectedItemPosition == 2) params["conditions"] = jsonOrderList

        return params
    }
}