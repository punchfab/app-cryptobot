package com.projects.fertrax.cryptobot

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import com.projects.fertrax.cryptobot.adapters.HistoryAdapter
import com.projects.fertrax.cryptobot.adapters.HistoryList
import kotlinx.android.synthetic.main.activity_history.*
import org.json.JSONObject

class HistoryActivity : Main2Activity() {
    private lateinit var historyList: MutableList<HistoryList>
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_history)

        rvHistory.layoutManager = LinearLayoutManager(this)
        historyList = mutableListOf()
        mHandler = Handler()

        fun getData() = Volley("https://kypo.gameplux.com/api.php?op=getHistory").getData(this, ::getAction)
        swrHistory.setOnRefreshListener {
            mRunnable = Runnable {
                historyList.clear()
                getData()
                swrHistory.isRefreshing = false
            }
            mHandler.postDelayed(
                    mRunnable,
                    300.toLong()
            )
        }
        getData()
    }

    override fun getLayoutId(): Int = R.layout.activity_history
    override fun getCustomTitle(): String? = "History"

    private fun getAction(obj: JSONObject) {
        val body = obj.getJSONArray("body")

        for (i in 0 until body.length()) {
            val objHistory = body.getJSONObject(i)
            val date = objHistory.getString("date")
            val profit = objHistory.getJSONObject("profit")
            val loss = objHistory.getJSONObject("loss")
            val total = objHistory.getJSONObject("total")

            val history = HistoryList(
                    date,
                    "${profit.getString("operations") } | ${String.format("%.2f", profit.getString("percentage").toDouble())}%",
                    "${loss.getString("operations")} | ${String.format("%.2f", loss.getString("percentage").toDouble())}%",
                    "${total.getString("operations")} | ${String.format("%.2f", total.getString("percentage").toDouble())}%")

            historyList.add(history)
            if (historyList.size > 0) rvHistory.adapter = HistoryAdapter(historyList)
        }
    }
}
