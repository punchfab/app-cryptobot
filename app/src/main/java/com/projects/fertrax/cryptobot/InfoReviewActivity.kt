package com.projects.fertrax.cryptobot

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_info_review.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject

class InfoReviewActivity : AppCompatActivity() {

    private var id: String = "0"
    private lateinit var symbol: String
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable
    private lateinit var locked: String

    private var mReceiver : BroadcastReceiver? = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            id = intent!!.getStringExtra("id")
            getData()
        }
    }
    private val mIntentFilter = IntentFilter("OPEN_INFO_ACTIVITY")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_review)
        supportActionBar!!.title = "Trade"
        if (intent.getStringExtra("id") != null) id = intent.getStringExtra("id")
        mHandler = Handler()

        swrInfo.setOnRefreshListener {
            mRunnable = Runnable {
                getData()
                swrInfo.isRefreshing = false
            }
            mHandler.postDelayed(
                    mRunnable,
                    300.toLong()
            )
        }
        getData()
    }

    override fun onResume() {
        super.onResume()
        Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=info&option=1&id=${intent.getStringExtra("id")}")
                .getData(this){}
        registerReceiver(mReceiver,mIntentFilter)
    }

    override fun onPause() {
        Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=info&option=0")
                .getData(this){}
        if(mReceiver != null) {
            unregisterReceiver(mReceiver)
            mReceiver = null
        }
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                id = data!!.getStringExtra("id")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.info, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_open_orders -> {
                val intent = Intent(this, OpenOrderActivity::class.java)
                intent.putExtra("symbol",irSymbol.text)
                intent.putExtra("id", id)
                startActivity(intent)
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    fun getData() = Volley("https://kypo.gameplux.com/api.php?op=getOnly&id=$id").getData(this, ::action)

    private fun action(obj: JSONObject) {
        val body = obj.getJSONObject("body")
        symbol = body.getString("symbol")
        irSymbol.text = symbol
        irStatus.text = body.getString("status")
        val price = JSONObject(body.getString("price"))
        var perExit = "%"
        var perReview = "%"
        if (body.getString("percentage") != "null") {
            val per = JSONObject(body.getString("percentage"))
            if (per.has("last_price")) {
                val lastPer = per.getString("last_price").toDouble()
                irLastPer.text = String.format("%.2f", lastPer) + "%"
                if (lastPer < 0) irLastPer.setTextColor(Color.RED)
                else irLastPer.setTextColor(Color.GREEN)

            }
            if (per.has("stop_loss")) {
                val stopPer = per.getString("stop_loss").toDouble()
                irPerStop.text = String.format("%.2f", stopPer) + "%"
                if (stopPer < 0) irPerStop.setTextColor(Color.RED)
                else irPerStop.setTextColor(Color.GREEN)
            }
            if (per.has("exit")) {
                perExit = String.format("%.2f", per.getString("exit").toDouble()) + "%"
                if (per.getString("exit").toDouble() < 0) exitPrice.setTextColor(Color.RED)
                else exitPrice.setTextColor(Color.GREEN)
            }
            if (per.has("review")) {
                perReview = String.format("%.2f", per.getString("review").toDouble()) + "%"
                if (per.getString("review").toDouble() < 0) reviewPrice.setTextColor(Color.RED)
                else reviewPrice.setTextColor(Color.GREEN)
            }
            if (per.has("high")) {
                if (per.getJSONObject("high").has("entry")) {
                    val highEntry = per.getJSONObject("high").getString("entry").toDouble()
                    irPerHighEntry.text = String.format("%.2f", highEntry) + "%"
                    if (highEntry < 0) irPerHighEntry.setTextColor(Color.RED)
                    else irPerHighEntry.setTextColor(Color.GREEN)
                }
                if (per.getJSONObject("high").has("review")) {
                    val highReview = per.getJSONObject("high").getString("review").toDouble()
                    irPerHighReview.text = String.format("%.2f", highReview) + "%"
                    if (highReview < 0) irPerHighReview.setTextColor(Color.RED)
                    else irPerHighReview.setTextColor(Color.GREEN)
                }
            }
            if (per.has("low")) {
                if (per.getJSONObject("low").has("entry")) {
                    val lowEntry = per.getJSONObject("low").getString("entry").toDouble()
                    irPerLowEntry.text = String.format("%.2f", lowEntry) + "%"
                    if (lowEntry < 0) irPerLowEntry.setTextColor(Color.RED)
                    else irPerLowEntry.setTextColor(Color.GREEN)
                }
                if (per.getJSONObject("low").has("review")) {
                    val lowReview = per.getJSONObject("low").getString("review").toDouble()
                    irPerLowReview.text = String.format("%.2f", lowReview) + "%"
                    if (lowReview < 0) irPerLowReview.setTextColor(Color.RED)
                    else irPerLowReview.setTextColor(Color.GREEN)
                }
            }
        }
        addButtons()
        if (price.has("review")) reviewPrice.text = "${price.getString("review")} | $perReview"
        if (price.has("entry")) entryPrice.text = price.getString("entry")
        if (price.has("exit")) exitPrice.text = "${price.getString("exit")} | $perExit"
        if (price.has("last")) irLastPrice.text = price.getString("last")
        if (price.has("high")) irPriceHigh.text = price.getString("high")
        if (price.has("low")) irPriceLow.text = price.getString("low")
        if (price.has("entryTime")) irStartTime.text = price.getString("entryTime")
        if (price.has("exitTime")) irExitTime.text = price.getString("exitTime")

        irElapsed.text = "${body.getJSONObject("diffTime").getString("create")} | ${body.getJSONObject("diffTime").getString("entry")}"
        irPriceStop.text = body.getString("stop_loss")
        irCreated.text = body.getString("created_at")
        if (body.getString("updated_at") != "null") irUpdated.text = body.getString("updated_at")

        val confirm = JSONObject(body.getString("confirm"))
        if (confirm.has("rsi-20")) {
            val rsi20 = JSONObject(confirm.getString("rsi-20"))
            t1.visibility = LinearLayout.VISIBLE
            if (rsi20.has("status")) {
                t1Status.text = rsi20.getString("status")
            }
            if (rsi20.has("position")) {
                t1Position.text = rsi20.getString("position")
            }
            if (rsi20.has("values"))
            {
                t1Values.text = rsi20.getString("values")
            }
            if (rsi20.has("startTime")) {
                t1StartTime.text = rsi20.getString("startTime")
            }
            if (rsi20.has("endTime")) {
                t1EndTime.text = rsi20.getString("endTime")
            }
        } else {
            indicator.removeView(t1)
        }

        if (confirm.has("rsi-30")) {
            t2.visibility = LinearLayout.VISIBLE
            val rsi30 = JSONObject(confirm.getString("rsi-30"))

            if (rsi30.has("status")) {
                t2Status.text = rsi30.getString("status")
            }
            if (rsi30.has("position")) {
                t2Position.text = rsi30.getString("position")
            }
            if (rsi30.has("values"))
            {
                t2Values.text = rsi30.getString("values")
            }
            if (rsi30.has("startTime")) {
                t2StartTime.text = rsi30.getString("startTime")
            }
            if (rsi30.has("endTime")) {
                t2EndTime.text = rsi30.getString("endTime")
            }
        } else {
            indicator.removeView(t2)
        }
        if (confirm.has("rsi-40")) {
            t3.visibility = LinearLayout.VISIBLE
            val rsi40 = JSONObject(confirm.getString("rsi-40"))

            if (rsi40.has("status")) {
                t3Status.text = rsi40.getString("status")
            }
            if (rsi40.has("position")) {
                t3Position.text = rsi40.getString("position")
            }
            if (rsi40.has("values"))
            {
                t3Values.text = rsi40.getString("values")
            }
            if (rsi40.has("startTime")) {
                t3StartTime.text = rsi40.getString("startTime")
            }
            if (rsi40.has("endTime")) {
                t3EndTime.text = rsi40.getString("endTime")
            }
        } else {
            indicator.removeView(t3)
        }
        if (confirm.has("stoch")){
            t4.visibility = LinearLayout.VISIBLE
            val stoch = JSONObject(confirm.getString("stoch"))

            if (stoch.has("status")) {
                t4Status.text = stoch.getString("status")
            }
            if (stoch.has("position")) {
                t4Position.text = stoch.getString("position")
            }
            if (stoch.has("values"))
            {
                val values = JSONObject(stoch.getString("values"))
                if (values.has("k") && values.has("d"))
                    t4Values.text = String.format("%.2f", values.getString("k").toDouble()) + "k / " + String.format("%.2f", values.getString("d").toDouble()) + "d"
            }
            if (stoch.has("startTime")) {
                t4StartTime.text = stoch.getString("startTime")
            }
            if (stoch.has("endTime")) {
                t4EndTime.text = stoch.getString("endTime")
            }
        } else {
            indicator.removeView(t4)
        }
        if (confirm.has("double-ema")) {
            t5.visibility = LinearLayout.VISIBLE
            val de = JSONObject(confirm.getString("double-ema"))

            if (de.has("status")) {
                t5Status.text = de.getString("status")
            }
            if (de.has("position")) {
                t5Position.text = de.getString("position")
            }
            if (de.has("values"))
            {
                t5Values.text = de.getString("values")
            }
            if (de.has("startTime")) {
                t5StartTime.text = de.getString("startTime")
            }
            if (de.has("endTime")) {
                t5EndTime.text = de.getString("endTime")
            }
        } else {
            indicator.removeView(t5)
        }
    }

    private fun addButtons() {
        val lbuttons = irButtons
        val btnParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        lbuttons.removeAllViews()
        when (irStatus.text) {
            "started" -> {
                val btnSell = Button(this)
                btnSell.layoutParams = btnParams
                btnSell.text = "Sell"
                lbuttons.addView(btnSell)

                btnSell.setOnClickListener {
                    val intent = Intent(applicationContext, SellActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("symbol",symbol)
                    startActivity(intent)
                }
            }
            "review" -> {
                val btnBuy = Button(this)
                val btnCancel = Button(this)
                btnBuy.layoutParams = btnParams
                btnCancel.layoutParams = btnParams
                btnBuy.text = "Buy"
                btnCancel.text = "Cancel"
                lbuttons.addView(btnBuy)
                lbuttons.addView(btnCancel)

                btnBuy.setOnClickListener {
                    val intent = Intent(applicationContext, BuyActivity::class.java)
                    intent.putExtra("id", id)
                    intent.putExtra("symbol",symbol)
                    startActivity(intent)
                }

                btnCancel.setOnClickListener {
                    val bView = layoutInflater.inflate(R.layout.dialog_block,null)
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Do you want to cancel this operation?")
                    builder.setView(bView)
                    builder.setPositiveButton("Yes"){ _, _ ->
                        val chBlock: CheckBox = bView.findViewById(R.id.chxCoinCancel)
                        locked = if (chBlock.isChecked) "1" else "0"
                        Volley("https://kypo.gameplux.com/api.php?op=reviewCancel&id=$id").sendData(this, setParams = {
                            val params = HashMap<String,String>()
                            params["block"] = locked
                            params["symbol"] = irSymbol.text.toString()
                            params
                        })
                        getData()
                    }
                    builder.setNegativeButton("No"){ _, _ ->

                    }
                    builder.show()

                    /*val intent = Intent(applicationContext, CancelActivity::class.java)
                    intent.putExtra("id", id)
                    startActivity(intent)*/
                }
            }
            else -> {
                lbuttons.visibility = LinearLayout.GONE
            }
        }
    }
}
