package com.projects.fertrax.cryptobot

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlin.reflect.KClass

abstract class Main2Activity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = getCustomTitle()

        layoutInflater.inflate(getLayoutId(), main_content)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    abstract fun getLayoutId(): Int

    abstract fun getCustomTitle(): String?

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main2, menu)
        return false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_add_coin -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> launchActivity(MainActivity::class.java)

            R.id.nav_review -> launchActivity(ReviewActivity::class.java)

            R.id.nav_history -> launchActivity(HistoryActivity::class.java)

            R.id.nav_alarm -> launchActivity(AlarmsActivity::class.java)

            R.id.nav_block -> launchActivity(BlockActivity::class.java)

            R.id.nav_manage -> launchActivity(SettingsActivity::class.java)
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun launchActivity(_class: Class<*>){

        if (this.javaClass.kotlin != _class) {
            startActivity(makePageIntent(_class))
            overridePendingTransition(0,0)
            finish()
        }
    }

    private fun makePageIntent(_class: Class<*>) : Intent{
        val intent = Intent(this, _class)
        //intent
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        return intent
    }
}
