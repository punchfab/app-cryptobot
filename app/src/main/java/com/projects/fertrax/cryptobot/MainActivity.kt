package com.projects.fertrax.cryptobot

import android.content.*
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import android.support.v7.app.AlertDialog


class MainActivity : Main2Activity() {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable
    private var mReceiver : BroadcastReceiver? = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            getData()
        }
    }
    private val mIntentFilter = IntentFilter("OPEN_HOME_ACTIVITY")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure!")
        builder.setMessage("Do you want to close the app?")
        builder.setPositiveButton("Yes"){ _, _ ->
            finish()
        }
        builder.setNegativeButton("No"){ _, _ -> }

        tvProfit.setOnClickListener { builder.show() }

        mHandler = Handler()
        swrHome.setOnRefreshListener {
            mRunnable = Runnable {
                getData()
                swrHome.isRefreshing = false
            }
            mHandler.postDelayed(
                    mRunnable,
                    300.toLong()
            )
        }
        getData()
    }

    override fun onResume() {
        super.onResume()
        Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=home&option=1")
                .getData(this){}
        registerReceiver(mReceiver,mIntentFilter)
    }

    override fun onPause() {
        Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=home&option=0")
                .getData(this){}
        if(mReceiver != null) {
            unregisterReceiver(mReceiver)
            mReceiver = null
        }
        super.onPause()
    }

    fun getData() = Volley("https://kypo.gameplux.com/api.php?op=getHome").getData(this, ::getAction)

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun getCustomTitle(): String? {
        return "Home"
    }

    private fun getAction(obj: JSONObject){
        val body = obj.getJSONObject("body")
        val profit = body.getJSONObject("profit")
        val loss = body.getJSONObject("loss")
        val balances = body.getJSONObject("balances")
        val totalOperations: Int = profit.getString("operations").toInt() + loss.getString("operations").toInt()
        val totalPercentage: Double = profit.getString("percentage").toDouble() + loss.getString("percentage").toDouble()

        tvProfit.text = "${profit.getString("operations")} / ${String.format("%.2f",profit.getString("percentage").toDouble())}%"
        tvLoss.text = "${loss.getString("operations")} / ${String.format("%.2f",loss.getString("percentage").toDouble())}%"
        tvTotal.text = "$totalOperations / ${String.format("%.2f",totalPercentage)}%"
        tvBtcTotal.text = balances.getString("btc_total")
        tvBtcAvailable.text = balances.getString(("btc_available"))
        tvUsdTotal.text = balances.getString("usd_total")
        tvBtcPrice.text = balances.getString("price")
    }
}
