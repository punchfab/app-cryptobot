package com.projects.fertrax.cryptobot

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging

class MyFirebaseInstanceIDService: FirebaseInstanceIdService() {

    private val TAG = "IdService"
    override fun onTokenRefresh() {
        super.onTokenRefresh()

        val refreshedToken = FirebaseInstanceId.getInstance().token
        FirebaseMessaging.getInstance().subscribeToTopic("cyptobot")

        Log.d(TAG, "IdToken: $refreshedToken")
    }
}