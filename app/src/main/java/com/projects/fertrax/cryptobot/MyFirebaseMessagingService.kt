package com.projects.fertrax.cryptobot

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.opengl.Visibility
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.*

class MyFirebaseMessagingService : FirebaseMessagingService() {

    val tag = "Service"

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        //super.onMessageReceived(remoteMessage)
        val data = remoteMessage!!.data
        Log.d(tag, "From: " + remoteMessage!!.from)
        //Log.d(tag, "Notification Message Body: " + remoteMessage.notification!!.body!!)
        Log.d(tag,"Notification Data:" + remoteMessage.data)

        //sendNotification(remoteMessage.notification!!.title!!, remoteMessage.notification!!.body!!, remoteMessage.data)
        if (data["id"] != "0" && data["status"] == "started" || data["status"] == "finished") {
            val broadcast = Intent()
            broadcast.putExtra("id",data["id"])
            broadcast.action = "OPEN_INFO_ACTIVITY"
            sendBroadcast(broadcast)
        }

        when (data["activity"]) {
            "home" -> {
                val broadcast = Intent()
                broadcast.action = "OPEN_HOME_ACTIVITY"
                sendBroadcast(broadcast)
            }
            /*"review" -> {
                val broadcast = Intent()
                broadcast.action = "OPEN_REVIEW_ACTIVITY"
                sendBroadcast(broadcast)
            }*/
            "info" -> {
                val broadcast = Intent()
                broadcast.action = "OPEN_INFO_ACTIVITY"
                broadcast.putExtra("id",data["id"])
                sendBroadcast(broadcast)
            }
        }

        if (data["activity"] != "home" && data["activity"] != "review" && data["activity"] != "info") {
            if (data["status"] == "review") {
                val broadcast = Intent()
                broadcast.action = "OPEN_REVIEW_ACTIVITY"
                sendBroadcast(broadcast)
            }
            sendNotification(remoteMessage.data)
        }
    }

    private fun sendNotification(data:Map<String,String>) {
        val act = if (data["id"] == "0") ReviewActivity::class.java else InfoReviewActivity::class.java
        val intent = Intent(this,act)
        intent.putExtra("id",data["id"])
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, "TRADING")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(data["title"])
                .setContentText(data["body"] +" id:"+data["id"])
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setLights(Color.RED, 3000,3000)
                .setContentIntent(pendingIntent)

        if (data["status"] == "review" || data["id"]!!.toInt()  == 0) {
            notificationBuilder.priority = NotificationCompat.PRIORITY_MIN
            notificationBuilder.setVibrate(longArrayOf(100,100,100,100))
            notificationBuilder.setAutoCancel(true)
        } else {
            notificationBuilder.priority = NotificationCompat.PRIORITY_MAX
            notificationBuilder.setVibrate(longArrayOf(1000,1000))
            notificationBuilder.setOngoing(true)
            notificationBuilder.setAutoCancel(true)
            notificationBuilder.setSound(defaultSoundUri)
        }
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val idNot = if (data["id"]!!.toInt() == 0) Random().nextInt() else data["id"]!!.toInt()
        notificationManager.notify(idNot,notificationBuilder.build())
    }
}