package com.projects.fertrax.cryptobot

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.projects.fertrax.cryptobot.adapters.OpenOrderAdapter
import com.projects.fertrax.cryptobot.adapters.OpenOrderList
import kotlinx.android.synthetic.main.activity_open_order.*

class OpenOrderActivity : AppCompatActivity() {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private lateinit var orderList: MutableList<OpenOrderList>
    lateinit var symbol: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_order)

        symbol = intent.getStringExtra("symbol")
        supportActionBar!!.title = symbol

        orderList = mutableListOf()
        orderReView.layoutManager = LinearLayoutManager(this)

        mHandler = Handler()
        swrOpenOrder.setOnRefreshListener {
            mRunnable = Runnable {
                orderList.clear()
                orderReView.adapter = OpenOrderAdapter(orderList)
                getData()
            }
            mHandler.postDelayed(
                    mRunnable,
                    300.toLong()
            )
        }
        getData()
    }

    private fun getData() {
        Volley("https://kypo.gameplux.com/api.php?op=openOrders&symbol=$symbol").getData(this){obj ->
            val body = obj.getJSONArray("body")
            for (i in 0 until body.length()) {
                val orderJson: HashMap<String,String> = gson.fromJson(body.getString(i), object : TypeToken<HashMap<String, String>>() {}.type)
                val openOrder = OpenOrderList(orderJson)
                orderList.add(openOrder)
                orderReView.adapter = OpenOrderAdapter(orderList)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                // Respond to the action bar's Up/Home button
                val dat = Intent()
                dat.putExtra("id", intent.getStringExtra("id"))
                setResult(Activity.RESULT_OK, dat)
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
