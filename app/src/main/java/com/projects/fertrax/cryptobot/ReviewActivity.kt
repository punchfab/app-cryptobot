package com.projects.fertrax.cryptobot

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.NavigationView
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Adapter
import android.widget.ListView
import android.widget.Toast
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.projects.fertrax.cryptobot.adapters.CoinReview
import com.projects.fertrax.cryptobot.adapters.ReviewAdapter
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.app_bar_main2.*
import kotlinx.android.synthetic.main.dialog_add_coin.view.*
import kotlinx.android.synthetic.main.dialog_filter.view.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ReviewActivity : Main2Activity() {
    private lateinit var rvReview : RecyclerView
    private lateinit var coinList : MutableList<CoinReview>
    private lateinit var mHandler: Handler
    private lateinit var mRunnable:Runnable
    private val gson = GsonBuilder().setPrettyPrinting().create()
    private lateinit var symbolSelected: String
    private var filter = ""
    private var filterLimit = "20"
    private var review = true
    private var started = true
    private var finished = true
    private var canceled = false
    private var mReceiver : BroadcastReceiver? = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            coinList.clear()
            //rvReview.adapter = ReviewAdapter(coinList)
            getData()
        }
    }
    private val mIntentFilter = IntentFilter("OPEN_REVIEW_ACTIVITY")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_review)

        rvReview = findViewById(R.id.rvReview)
        rvReview.layoutManager = LinearLayoutManager(this)
        coinList = mutableListOf()
        mHandler = Handler()

        swiperefresh.setOnRefreshListener {
            mRunnable = Runnable {
                coinList.clear()
                rvReview.adapter = ReviewAdapter(coinList)
                getData()
                swiperefresh.isRefreshing = false
            }
            mHandler.postDelayed(
                    mRunnable,
                    300.toLong()
            )
        }

        //"https://kypo.gameplux.com/api.php?op=getAll"
        getData()
    }

    override fun onResume() {
        super.onResume()
        //Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=review&option=1").getData(this){}
        registerReceiver(mReceiver,mIntentFilter)
    }

    override fun onPause() {
        //Volley("https://kypo.gameplux.com/api.php?op=realtime&activity=review&option=0") .getData(this){}
        if(mReceiver != null) {
            unregisterReceiver(mReceiver)
            mReceiver = null
        }
        super.onPause()
    }

    fun getData() = Volley("https://kypo.gameplux.com/api.php?op=getAll$filter").getData(applicationContext, ::action)

    override fun getLayoutId(): Int = R.layout.activity_review

    override fun getCustomTitle(): String? = "Review"

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main2, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add_coin -> {
                val bView = layoutInflater.inflate(R.layout.dialog_add_coin,null)
                Volley("https://kypo.gameplux.com/api.php?op=getBlock").getData(this){obj ->
                    val symbol: HashMap<String,String> = gson.fromJson(obj.getString("symbols"), object : TypeToken<HashMap<String,String>>() {}.type)
                    val list = ArrayList(symbol.keys)
                    bView.spinnerAddCoin.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
                }
                val builder = AlertDialog.Builder(this)
                builder.setTitle("New Operation")
                builder.setView(bView)
                builder.setPositiveButton("Add"){ _, _ ->
                    symbolSelected = bView.spinnerAddCoin.selectedItem.toString()
                    var id = "0"
                    Volley("https://kypo.gameplux.com/api.php?op=addCoin").sendData(this,
                            {obj -> id = obj.getJSONObject("body").getString("id")},
                            {
                                val params = HashMap<String,String>()
                                params["symbol"] = symbolSelected
                                params
                            })
                    val intent = Intent(this, ReviewActivity::class.java)
                    intent.putExtra("id",id)
                    startActivity(intent)
                    //launchActivity(MainActivity::class.java)
                }
                builder.setNegativeButton("Cancel"){ _, _ ->
                }
                builder.show()
            }
            R.id.action_filter -> {
                filter = ""
                val bView = layoutInflater.inflate(R.layout.dialog_filter,null)
                bView.cxStatusReview.isChecked = review
                bView.cxStatusStarted.isChecked = started
                bView.cxStatusFinished.isChecked = finished
                bView.cxStatusCancel.isChecked = canceled
                bView.filterLimit.setText(filterLimit)
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Filter")
                builder.setView(bView)
                builder.setPositiveButton("Apply"){_,_ ->
                    if (bView.cxStatusCancel.isChecked) filter += "&canceled=1"
                    if (bView.cxStatusReview.isChecked) filter += "&review=1"
                    if (bView.cxStatusStarted.isChecked) filter += "&started=1"
                    if (bView.cxStatusFinished.isChecked) filter += "&finished=1"
                    filter += "&limit=${bView.filterLimit.text}"
                    Toast.makeText(this,filter,Toast.LENGTH_LONG).show()
                    coinList.clear()
                    rvReview.adapter = ReviewAdapter(coinList)
                    Volley("https://kypo.gameplux.com/api.php?op=getAll$filter").getData(applicationContext, ::action)
                }
                builder.setNegativeButton("Cancel"){_,_->}
                builder.show()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun action(obj: JSONObject){
        val array = obj.getJSONArray("body")
        val filter = obj.getJSONObject("filter")
        if (filter.has("limit")) filterLimit = filter.getString("limit")
        review = filter.has("review")
        started = filter.has("started")
        finished = filter.has("finished")
        canceled = filter.has("canceled")

        for (i in 0 until array.length()) {
            val objectCoin = array.getJSONObject(i)
            var per = "--"
            if (objectCoin.getString("percentage") != "null") {
                per = String.format("%.2f",JSONObject(objectCoin.getString("percentage")).getString("last_price").toDouble())
            }

            val coin = CoinReview(
                    objectCoin.getString("symbol"),
                    objectCoin.getString("status"),
                    objectCoin.getString("priority"),
                    per,
                    objectCoin.getString("id")
            )
            coinList.add(coin)
            rvReview.adapter = ReviewAdapter(coinList)
        }
    }
}
