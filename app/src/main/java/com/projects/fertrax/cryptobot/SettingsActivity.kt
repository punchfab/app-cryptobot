package com.projects.fertrax.cryptobot

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_settings.*
import org.json.JSONObject

class SettingsActivity : Main2Activity() {

    private lateinit var valueSetting: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_settings)

        Volley("https://kypo.gameplux.com/api.php?op=getSetting").getData(this, ::getAction)

        swAutoReview.setOnClickListener{
            valueSetting = swAutoReview.isChecked.toString()
            confirmDialog("1")
        }

        swAlarms.setOnClickListener {
            valueSetting = swAlarms.isChecked.toString()
            confirmDialog("2")
        }

        swTrading.setOnClickListener {
            valueSetting = swTrading.isChecked.toString()
            confirmDialog("3")
        }

        swTest.setOnClickListener {
            valueSetting = swTest.isChecked.toString()
            confirmDialog("4")
        }

        swMin.setOnClickListener {
            valueSetting = swMin.isChecked.toString()
            confirmDialog("5")
        }

        swOperations.setOnClickListener{
            valueSetting = swOperations.isChecked.toString()
            confirmDialog("6")
        }
    }

    override fun getLayoutId(): Int { return R.layout.activity_settings }

    override fun getCustomTitle(): String? { return "Settings" }

    private fun textTrading() {
        swTrading.text = if (swTrading.isChecked) "Trading ON" else "Trading OFF"
        swTest.text = if (swTest.isChecked) "Test ON" else "Test OFF"
    }

    private fun getAction(obj: JSONObject){
        val body = obj.getJSONArray("body")

        for (i in 0 until body.length()) {
            val objectSetting = body.getJSONObject(i)

            when(objectSetting.getString("set_key")) {
                "review" -> swAutoReview.isChecked = objectSetting.getString("value") == "1"
                "alarm" -> swAlarms.isChecked = objectSetting.getString("value") == "1"
                "trading" -> swTrading.isChecked = objectSetting.getString("value") == "1"
                "test" -> swTest.isChecked = objectSetting.getString("value") == "1"
                "minprice" -> swMin.isChecked = objectSetting.getString("value") == "1"
                "operations" -> swOperations.isChecked = objectSetting.getString("value") == "1"
            }
        }
        textTrading()
    }

    private fun setAction(): Map<String,String> {
        val params = HashMap<String, String>()
        params["value"] = if (valueSetting.toBoolean()) "1" else "0"

        return params
    }

    private fun confirmDialog (id:String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure!")
        builder.setMessage("Do you want to confirm the action?")
        builder.setPositiveButton("Yes"){ _, _ ->
            Volley("https://kypo.gameplux.com/api.php?op=setSetting&&id=$id").sendData(this, setParams = ::setAction)
            textTrading()
        }
        builder.setNegativeButton("No"){ _, _ ->
            when (id) {
                "1" -> swAutoReview.isChecked = !(valueSetting.toBoolean())
                "2" -> swAlarms.isChecked = !(valueSetting.toBoolean())
                "3" -> swTrading.isChecked = !(valueSetting.toBoolean())
                "4" -> swTest.isChecked = !(valueSetting.toBoolean())
                "5" -> swMin.isChecked = !(valueSetting.toBoolean())
                "6" -> swOperations.isChecked = !(valueSetting.toBoolean())
            }
            textTrading()
        }
        builder.show()
    }
}
