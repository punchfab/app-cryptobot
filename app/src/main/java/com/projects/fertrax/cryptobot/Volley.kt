package com.projects.fertrax.cryptobot

import android.app.PendingIntent.getActivity
import android.content.Context
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONException
import org.json.JSONObject

class Volley(private val url: String) {

    fun getData(context: Context, body: (JSONObject) -> Unit) {
        //"https://kypo.gameplux.com/api.php?op=getAll"
        val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener<String> { response ->
                    try {
                        val obj = JSONObject(response)
                        body(obj)
                        Toast.makeText(context, obj.getString("msg"), Toast.LENGTH_LONG).show()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener {
                    volleyError->
                    Toast.makeText(context, volleyError.message, Toast.LENGTH_LONG).show()})

        val requestQueue = Volley.newRequestQueue(context)
        requestQueue.add<String>(stringRequest)
    }

    fun sendData(context: Context, getParams: (JSONObject) -> Unit = {}, setParams: () -> Map<String,String> = {HashMap()}) {

        val stringRequest = object : StringRequest(Request.Method.POST, url,
                Response.Listener<String> { response ->
                    try {
                        val obj = JSONObject(response)
                        getParams(obj)
                        Toast.makeText(context, obj.getString("msg"), Toast.LENGTH_LONG).show()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { error ->
                    Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
                }) {

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                //val params = HashMap<String, String>()
                return setParams()
            }
        }

        VolleySingleton.instance?.addToRequestQueue(stringRequest)
    }
}