package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.projects.fertrax.cryptobot.R
import com.projects.fertrax.cryptobot.Volley

class AlarmAdapter(private val alarms: MutableList<AlarmList>): RecyclerView.Adapter<AlarmAdapter.ViewHolder>() {
    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.coin_alarm, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.coin.text = alarms[position].coin
        holder.rule.text =
                if (alarms[position].values == "") "${alarms[position].rule} ${alarms[position].condition}"
                else "${alarms[position].rule} ${alarms[position].condition} ${alarms[position].values}"

        var infoData: AlarmList = alarms[position]
        holder.edit.setOnClickListener {
            Toast.makeText(context,alarms[position].id,Toast.LENGTH_SHORT).show()
        }
        var activeImage =
                if (alarms[position].active == "1") R.drawable.ic_notifications_active_black
                else R.drawable.ic_notifications_off_black

        holder.active.setImageResource(activeImage)

        holder.active.setOnClickListener{
            activeImage =
                    if (activeImage == R.drawable.ic_notifications_active_black) R.drawable.ic_notifications_off_black
                    else R.drawable.ic_notifications_active_black
            holder.active.setImageResource(activeImage)

            Volley("https://kypo.gameplux.com/api.php?op=alarm&id=${alarms[position].id}").sendData(context, setParams = {
                var setActive = "0"
                if (activeImage == R.drawable.ic_notifications_active_black) setActive = "1"
                val params = HashMap<String,String>()
                params["action"] = "edit"
                params["active"] =  setActive
                params
            })

        }

        holder.delete.setOnClickListener {
            var pos = alarms.indexOf(infoData)
            Volley("https://kypo.gameplux.com/api.php?op=alarm&&id=${alarms[pos].id}").sendData(context, setParams = ::deleteAction)

            if (pos >= 0) {
                alarms.removeAt(pos)
                notifyItemRemoved(pos)
            }
        }
    }

    override fun getItemCount() = alarms.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val coin: TextView = itemView.findViewById(R.id.ca_coin)
        val rule: TextView = itemView.findViewById(R.id.ca_rule)
        val edit: ImageButton = itemView.findViewById(R.id.ca_edit)
        val active: ImageButton = itemView.findViewById(R.id.ca_active)
        val delete: ImageButton = itemView.findViewById(R.id.ca_delete)
    }

    private fun deleteAction(): Map<String,String> {
        val params = HashMap<String, String>()
        params["action"] = "delete"

        return params
    }
}

class AlarmList (val coin: String,val rule: String, val condition: String, val values: String = "0",val active: String, val id: String = "0")