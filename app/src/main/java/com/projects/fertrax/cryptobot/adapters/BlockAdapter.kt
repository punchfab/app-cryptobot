package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.projects.fertrax.cryptobot.R
import com.projects.fertrax.cryptobot.Volley
import java.text.DateFormatSymbols

class BlockAdapter(private val coins: MutableList<BlockList>): RecyclerView.Adapter<BlockAdapter.ViewHolder>() {

    private lateinit var context : Context
    private lateinit var symbol: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.block_coins, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.coin.text = coins[position].name
        holder.price.text = coins[position].price
        var infoData: BlockList = coins[position]
        holder.delete.setOnClickListener {
            var pos = coins.indexOf(infoData)
            symbol = coins[pos].name
            Volley("https://kypo.gameplux.com/api.php?op=setBlock").sendData(context, setParams = ::deleteAction)

            if (pos >= 0) {
                coins.removeAt(pos)
                notifyItemRemoved(pos)
            }
        }
    }

    override fun getItemCount(): Int = coins.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val coin: TextView = itemView.findViewById(R.id.bcCoin)
        val price: TextView = itemView.findViewById(R.id.bcPrice)
        val delete: ImageButton = itemView.findViewById(R.id.bcDelete)
    }

    private fun deleteAction(): Map<String,String> {
        val params = HashMap<String, String>()
        params["action"] = "delete"
        params["symbol"] = symbol

        return params
    }
}

class BlockList (val name: String,val price: String)