package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.projects.fertrax.cryptobot.R

class HistoryAdapter(private val history: MutableList<HistoryList>): RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.coin_history, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.date.text = history[position].date
        holder.profit.text = history[position].profit
        holder.loss.text = history[position].loss
        holder.total.text = history[position].total
    }

    override fun getItemCount() = history.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date: TextView = itemView.findViewById(R.id.chDate)
        val profit: TextView = itemView.findViewById(R.id.chProfit)
        val loss: TextView = itemView.findViewById(R.id.chLoss)
        val total: TextView = itemView.findViewById(R.id.chTotal)
    }
}

class HistoryList (val date: String, val profit: String, val loss:String, val total: String)