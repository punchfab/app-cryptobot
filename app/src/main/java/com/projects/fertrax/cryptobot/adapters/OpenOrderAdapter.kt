package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.projects.fertrax.cryptobot.R
import kotlinx.android.synthetic.main.open_orders.view.*

class OpenOrderAdapter(private val orders: MutableList<OpenOrderList>): RecyclerView.Adapter<OpenOrderAdapter.ViewHolder>() {
    private lateinit var context : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.open_orders, parent, false)
        context = parent.context

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.orderId.text = orders[position].order["orderId"]
        holder.price.text = orders[position].order["price"]
        holder.origQty.text = orders[position].order["origQty"]
        holder.exQty.text = orders[position].order["executedQty"]
        holder.quoteQty.text = orders[position].order["cummulativeQuoteQty"]
        holder.status.text = orders[position].order["status"]
        holder.type.text = orders[position].order["type"]
        holder.side.text = orders[position].order["side"]
        holder.stopPrice.text = orders[position].order["stopPrice"]
        holder.iceQty.text = orders[position].order["icebergQty"]
        holder.time.text = orders[position].order["time"]
        holder.elapsed.text = orders[position].order["elapsed"]
    }

    override fun getItemCount() = orders.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val orderId: TextView = itemView.findViewById(R.id.orderId)
        val price: TextView = itemView.findViewById(R.id.orderPrice)
        val origQty: TextView = itemView.findViewById(R.id.orderOrigQty)
        val exQty: TextView = itemView.findViewById(R.id.orderExQty)
        val quoteQty: TextView = itemView.findViewById(R.id.orderQuoteQty)
        val status: TextView = itemView.findViewById(R.id.orderStatus)
        val type: TextView = itemView.findViewById(R.id.orderType)
        val side: TextView = itemView.findViewById(R.id.orderSide)
        val stopPrice: TextView = itemView.findViewById(R.id.orderStopPrice)
        val iceQty: TextView = itemView.findViewById(R.id.orderIceQty)
        val time: TextView = itemView.findViewById(R.id.orderTime)
        val elapsed: TextView = itemView.findViewById(R.id.orderElapsed)
    }
}

class OpenOrderList (val order: Map<String,String>)