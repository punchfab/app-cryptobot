package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.projects.fertrax.cryptobot.R

class OrderListAdapter(private val conditions: MutableList<OrderList>): RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {

    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.indicator_rules, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = conditions[position].indicator
        holder.rule.text =
                if (conditions[position].values == "0") conditions[position].rule
                else "${conditions[position].rule} ${conditions[position].values}"

        var infoData: OrderList = conditions[position]
        holder.delete.setOnClickListener {
            var pos = conditions.indexOf(infoData)

            if (pos >= 0) {
                conditions.removeAt(pos)
                notifyItemRemoved(pos)
            }
        }
    }

    override fun getItemCount() = conditions.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.ir_name)
        val rule: TextView = itemView.findViewById(R.id.ir_rule)
        val edit: ImageButton = itemView.findViewById(R.id.ir_edit)
        val delete: ImageButton = itemView.findViewById(R.id.ir_delete)
    }
}

class OrderList (val indicator: String, val rule: String, val id: String = "1", val values: String = "0")