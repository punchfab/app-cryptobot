package com.projects.fertrax.cryptobot.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.projects.fertrax.cryptobot.InfoReviewActivity
import com.projects.fertrax.cryptobot.R

class ReviewAdapter(private val coins: List<CoinReview>) : RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {

    private lateinit var context : Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.coin_review, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.symbol.text = coins[position].symbol
        holder.status.text = coins[position].status
        holder.priority.text = coins[position].priority
        holder.percentage.text = coins[position].percentage
        if (coins[position].percentage != "--" && coins[position].percentage.toDouble() < 0) holder.percentage.setTextColor(Color.RED)
        else holder.percentage.setTextColor(Color.GREEN)

        holder.itemView.setOnClickListener{
            val intent = Intent(context, InfoReviewActivity::class.java)
            intent.putExtra("id", coins[position].id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = coins.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val symbol: TextView = itemView.findViewById(R.id.crSymbol)
        val status: TextView = itemView.findViewById(R.id.crStatus)
        val priority: TextView = itemView.findViewById(R.id.crPriority)
        val percentage: TextView = itemView.findViewById(R.id.crPercentage)
    }
}

class CoinReview (val symbol: String, val status: String, val priority: String, val percentage: String, val id: String)
